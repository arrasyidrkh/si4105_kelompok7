package com.example.promotin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.promotin.Model.Project;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class DetailProjectAct extends AppCompatActivity {

    private Context context;

    private TextView tvNamaProject, tvKategori, tvFee, tvOwner, tvOwnerEmail, tvDeskripsi;
    private ImageView ivFotoProduk, ivFotoOwner;
    private Button btnAjukan;

    private FirebaseAuth firebaseAuth;

    private DatabaseReference databaseReferenceOwner, databaseReferenceProject, databaseReferenceProjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_project);
        initViews();

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReferenceOwner = FirebaseDatabase.getInstance().getReference("Users").child("UMKM");
        databaseReferenceProject = FirebaseDatabase.getInstance().getReference("project");
        databaseReferenceProjectRequest = FirebaseDatabase.getInstance().getReference("project_request");

        final String idProject = getIntent().getStringExtra("idProject");

        databaseReferenceProject.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    final Project project = dataSnapshot1.getValue(Project.class);
                    if (project.getId().equals(idProject)){
                        tvNamaProject.setText(project.getNama());
                        tvKategori.setText(project.getKategori());
                        tvFee.setText("Rp "+project.getFee());
                        tvOwner.setText(project.getOwner());
                        tvOwnerEmail.setText(project.getEmail());
                        tvDeskripsi.setText(project.getDeskripsi());
                        Picasso.with(context)
                                .load(project.getUrlFoto())
                                .fit()
                                .centerCrop()
                                .into(ivFotoProduk);

                        databaseReferenceOwner.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot dataSnapshot2 : dataSnapshot.getChildren()){
                                    UserUMKM owner = dataSnapshot2.getValue(UserUMKM.class);
                                    if (project.getEmail().equals(owner.getEmail())){
                                        Picasso.with(context)
                                                .load(owner.getPhotoURL())
                                                .fit()
                                                .centerCrop()
                                                .into(ivFotoOwner);
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        btnAjukan.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String idRequest = databaseReferenceProjectRequest.push().getKey();
                                ProjectRequest projectRequest = new ProjectRequest(idRequest, firebaseAuth.getCurrentUser().getEmail(), project.getEmail(), project.getId(), "Pending");
                                databaseReferenceProjectRequest.child(idRequest).setValue(projectRequest);

                                Intent intent = new Intent(DetailProjectAct.this, SuccessRequestProjectAct.class);
                                intent.putExtra("successMessage", "Berhasil mengajukan request project!");
                                startActivity(intent);
                                ListProjectAct.fa.finish();
                                finish();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void initViews(){
        tvNamaProject = findViewById(R.id.tv_detailproject_namaproject);
        tvKategori = findViewById(R.id.tv_detailproject_kategori);
        tvFee = findViewById(R.id.tv_detailproject_fee);
        tvOwner = findViewById(R.id.tv_detailproject_namaowner);
        tvOwnerEmail = findViewById(R.id.tv_detailproject_emailowner);
        tvDeskripsi = findViewById(R.id.tv_detailproject_deskripsiproject);

        ivFotoOwner = findViewById(R.id.iv_detailproject_fotoowner);
        ivFotoProduk = findViewById(R.id.iv_detailproject_foto);

        btnAjukan = findViewById(R.id.btn_detailproject_daftar);
    }
}
