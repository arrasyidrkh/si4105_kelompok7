package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class HomepageInfluencerAct extends AppCompatActivity {

    LinearLayout btn_list, btn_chat, btn_profile, btn_projects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage_influencer);

        btn_list = findViewById(R.id.btn_list);
        btn_chat = findViewById(R.id.btn_chat);
        btn_profile = findViewById(R.id.btn_profile);
        btn_projects = findViewById(R.id.btn_projects);

        btn_projects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomepageInfluencerAct.this, ListProjectAct.class));
            }
        });

        btn_chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomepageInfluencerAct.this, MainChatActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomepageInfluencerAct.this, MyProfileInfluencerAct.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
