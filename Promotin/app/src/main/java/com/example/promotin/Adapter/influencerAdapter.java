package com.example.promotin.Adapter;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.promotin.DetailinfluencerActivity;
import com.example.promotin.Model.Project;
import com.example.promotin.R;

import java.util.List;

public class influencerAdapter extends RecyclerView.Adapter<influencerAdapter.MyHolder> {


    List<Project> list;
    private Context context;
    public influencerAdapter(List<Project> noteslist, Context context)
    {
        this.context=context;
        this.list=noteslist;
    }


    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_influencer,viewGroup,false);

        MyHolder myHolder=new MyHolder(view);
        return myHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, int position) {
        final Project data=list.get(position);
        myHolder.nama.setText(data.getNama());
        myHolder.owner.setText(data.getOwner());
        myHolder.fee.setText(data.getFee());

        myHolder.card_article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent main = new Intent(context.getApplicationContext(), DetailinfluencerActivity.class);
                main.putExtra(DetailinfluencerActivity.EXTRA_NAMA, data.getNama());
                main.putExtra(DetailinfluencerActivity.EXTRA_DESK, data.getDeskripsi());
                main.putExtra(DetailinfluencerActivity.EXTRA_EMAIL, data.getEmail());
                main.putExtra(DetailinfluencerActivity.EXTRA_FEE, data.getFee());
                main.putExtra(DetailinfluencerActivity.EXTRA_ID, data.getId());
                main.putExtra(DetailinfluencerActivity.EXTRA_KATEGORI, data.getKategori());
                main.putExtra(DetailinfluencerActivity.EXTRA_OWNER, data.getOwner());
                main.putExtra(DetailinfluencerActivity.EXTRA_URL, data.getUrlFoto());
                context.startActivity(main);
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class  MyHolder extends RecyclerView.ViewHolder  {
        TextView nama,owner,fee;
        CardView card_article;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            nama=itemView.findViewById(R.id.product_card);
            owner=itemView.findViewById(R.id.nama_incluencer_card);
            fee=itemView.findViewById(R.id.jumlah_upload_card);
            card_article=itemView.findViewById(R.id.card_article);



        }


    }


}
