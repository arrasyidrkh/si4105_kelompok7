package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;

import com.example.promotin.Adapter.projectumkmAdapter;
import com.example.promotin.Model.Project;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListProjectumkm extends AppCompatActivity {
    private RecyclerView rvAcc,rvPending;
    private ProjectAdapter projectAdapter;

    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private List<Project> projects;

    public static Activity fa;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_projectumkm);

        rvAcc = findViewById(R.id.rv_Acc);


        rvAcc.setHasFixedSize(true);
        rvAcc.setLayoutManager(new LinearLayoutManager(this));

        final FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();

        projects = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("project");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                projects.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Project project = dataSnapshot1.getValue(Project.class);
                    projects.add(project);
                }

                projectAdapter = new ProjectAdapter(ListProjectumkm.this, projects);
                rvAcc.setAdapter(projectAdapter);
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }
}
