package com.example.promotin.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.promotin.DetailProjectAct;
import com.example.promotin.Model.Project;
import com.example.promotin.ProjectAdapter;
import com.example.promotin.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class projectumkmAdapter extends RecyclerView.Adapter<projectumkmAdapter.ProjectViewHolder> {

    private Context context;
    private List<Project> projects;

    public projectumkmAdapter(Context context, List<Project> projects) {
        this.context = context;
        this.projects = projects;
    }

    @NonNull
    @Override
    public ProjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.project_item, parent, false);
        return new ProjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull projectumkmAdapter.ProjectViewHolder holder, int position) {
        Project uploadCurrent = projects.get(position);
        holder.tvNama.setText(uploadCurrent.getNama());
        holder.tvOwner.setText(uploadCurrent.getOwner());
        //holder.tvFee.setText("Rp "+uploadCurrent.getFee());
        Picasso.with(context)
                .load(uploadCurrent.getUrlFoto())
                .fit()
                .centerCrop()
                .into(holder.ivFotoProduk);
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }

    public class ProjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tvNama, tvOwner, tvFee;
        public ImageView ivFotoProduk;

        public ProjectViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.tv_list_namaproject);
            tvOwner = itemView.findViewById(R.id.tv_list_owner);
            tvFee = itemView.findViewById(R.id.tv_list_fee);

            ivFotoProduk = itemView.findViewById(R.id.iv_list_foto);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Project project = projects.get(getAdapterPosition());

                    Intent intent = new Intent(v.getContext(), DetailProjectAct.class);
                    intent.putExtra("idProject", project.getId());
                    v.getContext().startActivity(intent);
                }
            });
        }

        @Override
        public void onClick(View v) {


        }
    }
}
