package com.example.promotin;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class DetailinfluencerActivity extends AppCompatActivity {

    public static final String EXTRA_DESK = "extra_desk";
    public static final String EXTRA_NAMA = "extra_nama";
    public static final String EXTRA_EMAIL = "extra_email";
    public static final String EXTRA_FEE = "extra_fee";
    public static final String EXTRA_ID = "extra_id";
    public static final String EXTRA_KATEGORI = "extra_kategori";
    public static final String EXTRA_OWNER = "extra_owner";
    public static final String EXTRA_URL = "extra_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailinfluencer);

        TextView tvNama = (TextView)findViewById(R.id.textView8);
        TextView tvkategori = (TextView)findViewById(R.id.textView_kategori);
        TextView tvFee = (TextView)findViewById(R.id.textfee_data);
        TextView tvEmail = (TextView)findViewById(R.id.textemail_data);
        TextView tvOwner = (TextView)findViewById(R.id.textowner_data);
        TextView tvdeskripsi = (TextView)findViewById(R.id.textdeskripsi_data);

        tvNama.setText(getIntent().getStringExtra(EXTRA_NAMA));
        tvkategori.setText(getIntent().getStringExtra(EXTRA_KATEGORI));
        tvFee.setText(getIntent().getStringExtra(EXTRA_FEE));
        tvEmail.setText(getIntent().getStringExtra(EXTRA_EMAIL));
        tvOwner.setText(getIntent().getStringExtra(EXTRA_OWNER));
        tvdeskripsi.setText(getIntent().getStringExtra(EXTRA_DESK));


    }
}
