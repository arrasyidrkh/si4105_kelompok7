package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class EditProfileInfActivity extends AppCompatActivity {

    ImageView photo_edit_profile;
    EditText xnama_lengkap, xhandphone, xinstagram, xusername;

    Uri photo_location;
    Integer photo_max = 1;

    FirebaseAuth firebaseAuth;
    DatabaseReference reference;
    StorageReference storage;

    String USERNAME_KEY = "usernamekey";
    String username_key = "";
    String username_key_new = "";

    Button btn_save, btn_add_new_photo;
    LinearLayout btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_inf);

        photo_edit_profile = findViewById(R.id.photo_edit_profile);

        btn_add_new_photo = findViewById(R.id.btn_add_new_photo);

        btn_save = findViewById(R.id.btn_save);
        btn_back = findViewById(R.id.btn_back);

        xnama_lengkap = findViewById(R.id.xnama_lengkap);
        xhandphone = findViewById(R.id.xhandphone);
        xinstagram = findViewById(R.id.xinstagram);
        xusername = findViewById(R.id.xusername);

        getUsernameLocal();

        final FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();

        reference = FirebaseDatabase.getInstance().getReference()
                .child("Users").child("influencer").child(firebaseUser.getUid());
        storage = FirebaseStorage.getInstance().getReference().child("users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                xnama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                xhandphone.setText(dataSnapshot.child("handphone").getValue().toString());
                xinstagram.setText(dataSnapshot.child("instagram").getValue().toString());
                xusername.setText(dataSnapshot.child("username").getValue().toString());

                if (dataSnapshot.hasChild("imageURL")) {
                    Picasso.with(EditProfileInfActivity.this)
                            .load(dataSnapshot.child("imageURL")
                                    .getValue().toString()).centerCrop().fit()
                            .into(photo_edit_profile);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // ubah state menjadi loading
                btn_save.setEnabled(false);
                btn_save.setText("Loading ...");

                // validasi untuk file (apakah ada?)
                if (photo_location != null){
                    final StorageReference storageReference1 =
                            storage.child(System.currentTimeMillis() + "." +
                                    getFileExtension(photo_location));

                    storageReference1.putFile(photo_location)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                    storageReference1.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            reference = FirebaseDatabase.getInstance().getReference("Users").child("influencer").child(firebaseUser.getUid());
                                            String uri_photo = uri.toString();

                                            HashMap<String, Object> map = new HashMap<>();
                                            map.put("imageURL", uri_photo);
                                            map.put("handphone", xhandphone.getText().toString());
                                            map.put("instagram", xinstagram.getText().toString());
                                            map.put("nama_lengkap", xnama_lengkap.getText().toString());
                                            map.put("username", xusername.getText().toString());

                                            reference.updateChildren(map);

                                        }
                                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            Intent gotosuccessedit = new Intent(EditProfileInfActivity.this,SuccessEditInfAct.class);
                                            startActivity(gotosuccessedit);
                                        }
                                    });
                                }
                            }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                            // berpindah activity
//                            Intent gotobackprofile = new Intent(EditProfileAct.this,MyProfileAct.class);
//                            startActivity(gotobackprofile);
                        }
                    });
                }



            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(EditProfileInfActivity.this,MyProfileInfluencerAct.class);
                startActivity(back);
            }
        });

        btn_add_new_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findPhoto();
            }
        });
    }

    String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    public void findPhoto(){
        Intent pic = new Intent();
        pic.setType("image/*");
        pic.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(pic, photo_max);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == photo_max && resultCode == RESULT_OK && data != null && data.getData() != null)
        {

            photo_location = data.getData();
            Picasso.with(this).load(photo_location).centerCrop().fit().into(photo_edit_profile);

        }

    }

    public void getUsernameLocal(){
        SharedPreferences sharedPreferences = getSharedPreferences(USERNAME_KEY, MODE_PRIVATE);
        username_key_new = sharedPreferences.getString(username_key, "");
    }
}
