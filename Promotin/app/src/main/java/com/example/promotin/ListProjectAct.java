package com.example.promotin;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.promotin.Model.Project;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListProjectAct extends AppCompatActivity {

    private RecyclerView rvListProject;
    private ProjectAdapter projectAdapter;

    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseAuth;
    private List<Project> projects;

    public static Activity fa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_project);

        fa = this;

        rvListProject = findViewById(R.id.rv_list_project);

        rvListProject.setHasFixedSize(true);
        rvListProject.setLayoutManager(new LinearLayoutManager(this));

        projects = new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference("project");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                projects.clear();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()){
                    Project project = dataSnapshot1.getValue(Project.class);
                    projects.add(project);
                }

                projectAdapter = new ProjectAdapter(ListProjectAct.this, projects);
                rvListProject.setAdapter(projectAdapter);
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
