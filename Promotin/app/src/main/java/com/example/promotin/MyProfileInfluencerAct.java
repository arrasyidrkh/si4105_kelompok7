package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class MyProfileInfluencerAct extends AppCompatActivity {

    Button btn_edit_profile, btn_back_home, btn_sign_out;

    TextView nama_lengkap, type, handphone, instagram, project;
    ImageView photo_profile;

    FirebaseUser firebaseUser;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_influencer);

        btn_edit_profile = findViewById(R.id.btn_edit_profile);
        btn_back_home = findViewById(R.id.btn_back_home);
        btn_sign_out = findViewById(R.id.btn_sign_out);

        nama_lengkap = findViewById(R.id.nama_lengkap);
        type = findViewById(R.id.type);
        handphone = findViewById(R.id.handphone);
        instagram = findViewById(R.id.instagram);
        project = findViewById(R.id.project);
        photo_profile = findViewById(R.id.photo_profile);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child("influencer").child(firebaseUser.getUid());
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
                type.setText(dataSnapshot.child("type").getValue().toString());
                handphone.setText(dataSnapshot.child("handphone").getValue().toString());
                instagram.setText(dataSnapshot.child("instagram").getValue().toString());
                project.setText(dataSnapshot.child("project").getValue().toString() + " projects");

                if (dataSnapshot.hasChild("imageURL")) {
                    Picasso.with(MyProfileInfluencerAct.this)
                            .load(dataSnapshot.child("imageURL")
                                    .getValue().toString()).centerCrop().fit()
                            .into(photo_profile);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        btn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoeditprofile = new Intent(MyProfileInfluencerAct.this, EditProfileInfActivity.class);
                startActivity(gotoeditprofile);
            }
        });

        btn_back_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goback = new Intent(MyProfileInfluencerAct.this,HomepageInfluencerAct.class);
                startActivity(goback);
            }
        });

        btn_sign_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();

                // berpindah activity
                Intent gotologin = new Intent(MyProfileInfluencerAct.this,LoginInfluencerAct.class);
                startActivity(gotologin);
                finish();
            }
        });
    }
}

