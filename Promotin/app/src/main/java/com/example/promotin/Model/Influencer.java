package com.example.promotin.Model;

public class Influencer {
    public int id;
    public String name, instagram;

    public Influencer() {

    }

    public Influencer(String name, String instagram){
        this.name = name;
        this.instagram = instagram;
    }
}
