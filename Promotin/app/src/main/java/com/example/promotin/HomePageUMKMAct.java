package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class HomePageUMKMAct extends AppCompatActivity {

    private LinearLayout btnInfluencers, btnChat, btnMakeProject, btnProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage_umkm);
        initViews();

        btnMakeProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomePageUMKMAct.this, MakeProjectAct.class));
            }
        });

        btnInfluencers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomePageUMKMAct.this, ListInfluencer.class));
            }
        });
    }

    public void initViews(){
        btnInfluencers = findViewById(R.id.btn_homum_listinfluencer);
        btnChat = findViewById(R.id.btn_homum_chat);
        btnMakeProject = findViewById(R.id.btn_homum_makeproject);
        btnProfile = findViewById(R.id.btn_homum_profile);
    }


    public void profiles(View view) {
        Intent maisn = new Intent(getApplicationContext(),UMKM_profile.class);
        startActivity(maisn);
    }

    public void listPro(View view) {
        Intent maisn = new Intent(getApplicationContext(),ListProjectumkm.class);
        startActivity(maisn);
    }
}
