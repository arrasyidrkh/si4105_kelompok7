package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

public class UMKM_profile extends AppCompatActivity {

    FirebaseAuth firebaseAuth;
    DatabaseReference reference;
    StorageReference storage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_umkm_profile);

        final TextView EdtNamaUMKM = (TextView)findViewById(R.id.textView8);
        final TextView tvEmail = (TextView)findViewById(R.id.textemailumkm_data);
        final TextView EdtJeniUMKM = (TextView)findViewById(R.id.textjenisumkm_data);

        final FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();



        reference = FirebaseDatabase.getInstance().getReference()
                .child("Users").child("UMKM").child(firebaseUser.getUid());
     //   storage = FirebaseStorage.getInstance().getReference().child("users");
     //   final Button button_save_edit_UMKM = (Button)findViewById(R.id.button_save_edit_UMKM);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                EdtJeniUMKM.setText(dataSnapshot.child("type").getValue().toString());
                EdtNamaUMKM.setText(dataSnapshot.child("nama_umkm").getValue().toString());
              //  tvEmail.setText(dataSnapshot.child("email").getValue().toString());
//                xnama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
//                xhandphone.setText(dataSnapshot.child("handphone").getValue().toString());
//                xinstagram.setText(dataSnapshot.child("instagram").getValue().toString());
//                xusername.setText(dataSnapshot.child("username").getValue().toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void EditPro(View view) {
        Intent main = new Intent(getApplicationContext(),EditProfilUmkm.class);
        startActivity(main);

    }

    public void backHome(View view) {
        onBackPressed();
    }
}
