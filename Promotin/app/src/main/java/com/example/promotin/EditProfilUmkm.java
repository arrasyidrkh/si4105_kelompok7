package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class EditProfilUmkm extends AppCompatActivity {
    FirebaseAuth firebaseAuth;
    DatabaseReference reference;
    StorageReference storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil_umkm);

        final EditText editText_NamaUMKMss = (EditText)findViewById(R.id.editText_NamaUMKM);
        final EditText editText_PemilikUMKMss = (EditText)findViewById(R.id.editText_PemilikUMKM);
        final EditText editText_JenisUMKMss = (EditText)findViewById(R.id.editText_JenisUMKM);

        final FirebaseUser firebaseUser = firebaseAuth.getInstance().getCurrentUser();
       // editText_NamaUMKMss.setText(firebaseUser.getUid());
        reference = FirebaseDatabase.getInstance().getReference()
                .child("Users").child("UMKM").child(firebaseUser.getUid());
        storage = FirebaseStorage.getInstance().getReference().child("users");
        final Button button_save_edit_UMKM = (Button)findViewById(R.id.button_save_edit_UMKM);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                editText_NamaUMKMss.setText(dataSnapshot.child("nama_umkm").getValue().toString());
                editText_PemilikUMKMss.setText(dataSnapshot.child("ownerUMKM").getValue().toString());
                editText_JenisUMKMss.setText(dataSnapshot.child("type").getValue().toString());
//                xnama_lengkap.setText(dataSnapshot.child("nama_lengkap").getValue().toString());
//                xhandphone.setText(dataSnapshot.child("handphone").getValue().toString());
//                xinstagram.setText(dataSnapshot.child("instagram").getValue().toString());
//                xusername.setText(dataSnapshot.child("username").getValue().toString());

                button_save_edit_UMKM.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        reference = FirebaseDatabase.getInstance().getReference("Users").child("UMKM").child(firebaseUser.getUid());
                        HashMap<String, Object> map = new HashMap<>();
                        map.put("nama_umkm", editText_NamaUMKMss.getText().toString());
                        map.put("ownerUMKM", editText_PemilikUMKMss.getText().toString());
                        map.put("type", editText_JenisUMKMss.getText().toString());

                        reference.updateChildren(map);


                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void backprof(View view) {
        onBackPressed();
    }
}
