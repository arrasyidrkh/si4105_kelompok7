package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ProfileInfluencerAct extends AppCompatActivity {

    Button btn_back;

    TextView nama_lengkap, type, handphone, instagram, project;
    ImageView photo_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_influencer);

        btn_back = findViewById(R.id.btn_back_home);

        nama_lengkap = findViewById(R.id.nama_lengkap);
        type = findViewById(R.id.type);
        handphone = findViewById(R.id.handphone);
        instagram = findViewById(R.id.instagram);
        project = findViewById(R.id.project);
        photo_profile = findViewById(R.id.photo_profile);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(ProfileInfluencerAct.this, ......);
//                startActivity(intent);
            }
        });
    }
}
