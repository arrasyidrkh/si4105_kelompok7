package com.example.promotin;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.promotin.Model.Influencer;

import java.util.ArrayList;

public class InfluencerListViewAdapter extends BaseAdapter {
    final ArrayList<Influencer> listInfluencer;

    InfluencerListViewAdapter(ArrayList<Influencer> listInfluencer) {
        this.listInfluencer = listInfluencer;
    }

    @Override
    public int getCount() {
        return listInfluencer.size();
    }

    @Override
    public Object getItem(int position) {
        return listInfluencer.get(position);
    }

    @Override
    public long getItemId(int position) {
        return listInfluencer.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View viewProduct;
        if (convertView == null) {
            viewProduct = View.inflate(parent.getContext(), R.layout.activity_influencer_view, null);
        } else viewProduct = convertView;


        Influencer product = (Influencer) getItem(position);
        ((TextView) viewProduct.findViewById(R.id.name)).setText(product.name);
        ((TextView) viewProduct.findViewById(R.id.followers)).setText(product.instagram);

        return viewProduct;
    }
}
