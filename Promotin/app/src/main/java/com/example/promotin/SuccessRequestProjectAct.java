package com.example.promotin;

import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SuccessRequestProjectAct extends AppCompatActivity {

    private Animation app_splash, btt, ttb;
    private ImageView ivImage;
    private TextView tvMessage;
    private Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_request_project);
        initViews();

        ivImage.startAnimation(app_splash);
        tvMessage.startAnimation(app_splash);
        btnOk.startAnimation(btt);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initViews(){
        ivImage = findViewById(R.id.iv_splash_image);
        tvMessage = findViewById(R.id.tv_splash_message);
        btnOk = findViewById(R.id.btn_splash_ok);

        app_splash = AnimationUtils.loadAnimation(this, R.anim.app_splash);
        btt = AnimationUtils.loadAnimation(this, R.anim.btt);
    }
}
