package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SelectTypeAct extends AppCompatActivity {

    public static Activity activity;

    private Button btnInfluencer, btnUMKM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_type);
        initViews();

        activity = this;

        btnUMKM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SelectTypeAct.this, LoginUMKMAct.class));
//            startActivity(new Intent(SelectTypeAct.this, ListInfluencer.class));
            }
        });

        btnInfluencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SelectTypeAct.this, LoginInfluencerAct.class));
            }
        });
    }

    public void initViews(){
        btnInfluencer = findViewById(R.id.button_influencer);
        btnUMKM = findViewById(R.id.button_umkm);
    }
}
