package com.example.promotin;

public class ProjectRequest {
    String id, influencerEmail, UMKMEmail, idProject, status;

    public ProjectRequest(){}

    public ProjectRequest(String id, String influencerEmail, String UMKMEmail, String idProject, String status) {
        this.id = id;
        this.influencerEmail = influencerEmail;
        this.UMKMEmail = UMKMEmail;
        this.idProject = idProject;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInfluencerEmail() {
        return influencerEmail;
    }

    public void setInfluencerEmail(String influencerEmail) {
        this.influencerEmail = influencerEmail;
    }

    public String getUMKMEmail() {
        return UMKMEmail;
    }

    public void setUMKMEmail(String UMKMEmail) {
        this.UMKMEmail = UMKMEmail;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
