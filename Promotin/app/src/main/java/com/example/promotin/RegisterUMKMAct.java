package com.example.promotin;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.HashMap;

public class RegisterUMKMAct extends AppCompatActivity {
    EditText jnamaUMKM, jemailUMKM, jpassword, jpemilikUMKM;
    Button jregistUMKM, btnPickPhoto;
    FirebaseAuth jfirebaseAuth;
    TextView jloginButton;

    private ImageView ivFotoProfil;
    private Uri imageUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReference, databaseReferenceUserChat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_umkm);

        jnamaUMKM = findViewById(R.id.namaUMKM);
        jemailUMKM = findViewById(R.id.emailUMKM);
        jpassword = findViewById(R.id.password);
        jpemilikUMKM = findViewById(R.id.pemilikUMKM);
        jregistUMKM = findViewById(R.id.registUMKM);
        jloginButton = findViewById(R.id.createText);
        ivFotoProfil = findViewById(R.id.iv_regum_foto2);
        btnPickPhoto = findViewById(R.id.btn_regum_pickphoto);


        jfirebaseAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference("users");

        btnPickPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPhoto();
            }
        });

        jregistUMKM.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String email = jemailUMKM.getText().toString().trim();
                String password = jpassword.getText().toString().trim();
                final String namaUMKM = jnamaUMKM.getText().toString().trim();
                final String ownerUMKM = jpemilikUMKM.getText().toString().trim();

                final StorageReference photoReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imageUri));

                if (TextUtils.isEmpty(email)) {
                    jemailUMKM.setError("Email is Required");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    jpassword.setError("Password is Required");
                    return;
                }
                if (TextUtils.isEmpty(namaUMKM)){
                    jnamaUMKM.setError("Nama UMKM is required");
                    return;
                }
                if (TextUtils.isEmpty(ownerUMKM)){
                    jpemilikUMKM.setError("Pemilik UMKM is required");
                }
                if (password.length() < 6) {
                    jpassword.setError("Password Must be More than 6 Characters");
                    return;
                }
                jregistUMKM.setText("Loading...");
                //regist use in firebase
                jfirebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            photoReference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    photoReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {

                                            databaseReference = FirebaseDatabase.getInstance().getReference("Users").child("UMKM");
                                            databaseReferenceUserChat = FirebaseDatabase.getInstance().getReference("UChat");

                                            FirebaseUser firebaseUser = jfirebaseAuth.getCurrentUser();
                                            String userId = firebaseUser.getUid();

                                            HashMap<String, String> hashMap = new HashMap<>();
                                            hashMap.put("id", userId);
                                            hashMap.put("project", "0");
                                            hashMap.put("type", "UMKM");
                                            hashMap.put("nama_umkm", namaUMKM);
                                            hashMap.put("handphone", "blank");
                                            hashMap.put("ownerUMKM", ownerUMKM);
                                            hashMap.put("photoURL", uri.toString());
                                            hashMap.put("status", "offline");
                                            hashMap.put("search", namaUMKM.toLowerCase());

                                            HashMap<String, String> hUsersChat = new HashMap<>();
                                            hUsersChat.put("id", userId);
                                            hUsersChat.put("username", namaUMKM);
                                            hUsersChat.put("imageURL", uri.toString());
                                            hUsersChat.put("status", "offline");

                                            databaseReferenceUserChat.child(userId).setValue(hUsersChat);
                                            databaseReference.child(userId).setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Intent intent = new Intent(RegisterUMKMAct.this, LoginUMKMAct.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                }
                                            });

                                        }
                                    });
                                }
                            });


                        } else {
                            Toast.makeText(RegisterUMKMAct.this, "Error! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                jloginButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(getApplicationContext(), LoginUMKMAct.class));
                    }
                });

            }
        });
    }

    private void pickPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            imageUri = data.getData();
            Picasso.with(this).load(imageUri).centerCrop().fit().into(ivFotoProfil);

        }
    }
}