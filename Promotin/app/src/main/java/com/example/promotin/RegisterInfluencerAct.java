package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

public class RegisterInfluencerAct extends AppCompatActivity {


    EditText mFullName, mEmail, mInstagramAccount, mPassword1, mUsername;
    Button mRegistButton, btnPickPhoto;
    TextView mLoginInfluencer;
    FirebaseAuth mAuth;
    DatabaseReference reference, databaseReferenceUserschat;


    private ImageView ivFotoProfil;

    private StorageReference storageReference;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_influencer);
        mFullName = findViewById(R.id.et_reginf_namalengkap);
        mUsername = findViewById(R.id.et_reginf_username);
        mEmail = findViewById(R.id.et_reginf_email);
        mInstagramAccount = findViewById(R.id.et_reginf_instagram);
        mPassword1 = findViewById(R.id.et_reginf_password);
        mRegistButton = findViewById(R.id.btn_reginf_register);
        mLoginInfluencer = findViewById(R.id.tv_reginf_login);
        btnPickPhoto = findViewById(R.id.btn_reginf_pickphoto);
        ivFotoProfil = findViewById(R.id.iv_reginf_foto2);

        mAuth = FirebaseAuth.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference("users");

        btnPickPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPhoto();
            }
        });

        mRegistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString().trim();
                String password = mPassword1.getText().toString().trim();
                final String username = mUsername.getText().toString().trim();
                final String nama_lengkap = mFullName.getText().toString().trim();
                final String instagram = mInstagramAccount.getText().toString().trim();

                final StorageReference photoReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imageUri));

                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Email is Required.");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    mPassword1.setError("Password is Required");
                    return;
                }
                if (password.length() < 5) {
                    mPassword1.setError("Password must be more than 5 Characters");
                    return;
                }
                if (imageUri == null){
                    Toast.makeText(RegisterInfluencerAct.this, "Harap pilih foto", Toast.LENGTH_SHORT).show();
                }
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(nama_lengkap) || TextUtils.isEmpty(instagram)) {
                    Toast.makeText(RegisterInfluencerAct.this, "All fields required", Toast.LENGTH_SHORT).show();
                    return;
                }
                mRegistButton.setText("Loading...");
                //regist user
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            photoReference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    photoReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            reference = FirebaseDatabase.getInstance().getReference("Users").child("influencer");
                                            databaseReferenceUserschat = FirebaseDatabase.getInstance().getReference("UChat");

                                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                                            String userid = firebaseUser.getUid();

                                            HashMap<String, String> hashMap = new HashMap<>();
                                            hashMap.put("id", userid);
                                            hashMap.put("project", "0");
                                            hashMap.put("type", "Influencer");
                                            hashMap.put("nama_lengkap", nama_lengkap);
                                            hashMap.put("instagram", instagram);
                                            hashMap.put("handphone", "blank");
                                            hashMap.put("username", username);
                                            hashMap.put("imageURL", uri.toString());
                                            hashMap.put("status", "offline");
                                            hashMap.put("search", username.toLowerCase());

                                            HashMap<String, String> hUsersChat = new HashMap<>();
                                            hUsersChat.put("id", userid);
                                            hUsersChat.put("username", nama_lengkap);
                                            hUsersChat.put("imageURL", uri.toString());
                                            hUsersChat.put("status", "offline");

                                            databaseReferenceUserschat.child(userid).setValue(hUsersChat);
                                            reference.child(userid).setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()){
                                                        Intent intent = new Intent(RegisterInfluencerAct.this, LoginInfluencerAct.class);
                                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            });


                        } else {
                            Toast.makeText(RegisterInfluencerAct.this, "Error! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        mLoginInfluencer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginInfluencerAct.class));
            }
        });
    }

    private void pickPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            imageUri = data.getData();
            Picasso.with(this).load(imageUri).centerCrop().fit().into(ivFotoProfil);

        }
    }
}

