package com.example.promotin;

import android.os.Bundle;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.promotin.Model.Influencer;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ListInfluencer extends AppCompatActivity {

    private Button buttonToSearch;
    private String keyword;

    ArrayList<Influencer> influencer;
    InfluencerListViewAdapter influencerListViewAdapter;
    ListView listView;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference influencerDatabase = database.getReference("Users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_influencer);

        buttonToSearch = findViewById(R.id.buttonToSearch);
        buttonToSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ListInfluencer.this, SearchInfluencer.class));
            }
        });

        Intent intent = getIntent();
        this.keyword = intent.getStringExtra("keyword");

        influencer = new ArrayList<>();

        System.out.println(this.keyword);

        if (this.keyword == null) {
            influencerDatabase.child("influencer").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        influencer.add(new Influencer(child.child("nama_lengkap").getValue(String.class), child.child("instagram").getValue(String.class)));
                    }

                    if (influencer.size() == 0) {
                        startActivity(new Intent(ListInfluencer.this, ListInfluencerEmpty.class));
                    }

                    listView = findViewById(R.id.listview);
                    influencerListViewAdapter = new InfluencerListViewAdapter(influencer);
                    listView.setAdapter(influencerListViewAdapter);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    startActivity(new Intent(ListInfluencer.this, ListInfluencerError.class));
                }
            });
        } else {
            influencerDatabase.child("influencer").orderByChild("nama_lengkap").equalTo(this.keyword).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        influencer.add(new Influencer(child.child("nama_lengkap").getValue(String.class), child.child("instagram").getValue(String.class)));
                    }

                    if (influencer.size() == 0) {
                        startActivity(new Intent(ListInfluencer.this, ListInfluencerNotFound.class));
                    }

                    listView = findViewById(R.id.listview);
                    influencerListViewAdapter = new InfluencerListViewAdapter(influencer);
                    listView.setAdapter(influencerListViewAdapter);
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    startActivity(new Intent(ListInfluencer.this, ListInfluencerError.class));
                }
            });
        }
    }
}
