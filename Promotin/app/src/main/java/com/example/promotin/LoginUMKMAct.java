package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginUMKMAct extends AppCompatActivity {
    EditText lemail, lpassword;
    Button lsignUMKM;
    FirebaseAuth lfirebaseAuth;
    TextView lcreateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_umkm);

        lemail = findViewById(R.id.emailUMKM);
        lpassword = findViewById(R.id.password);
        lsignUMKM = findViewById(R.id.signUMKM);
        lfirebaseAuth = FirebaseAuth.getInstance();
        lcreateText = findViewById(R.id.tv_login_createText);

        lsignUMKM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = lemail.getText().toString().trim();
                String password = lpassword.getText().toString().trim();

                if(TextUtils.isEmpty(email)) {
                    lemail.setError("Email is Required");
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    lpassword.setError("Password is Required");
                    return;
                }
                if(password.length() < 6){
                    lpassword.setError("Password Must be More than 6 Characters");
                    return;
                }
                lsignUMKM.setText("Loading...");
                //authenticate the user
                lfirebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(LoginUMKMAct.this, "Logged in Successfull", Toast.LENGTH_SHORT).show();
                            //startActivity(new Intent(getApplicationContext(), HomePageUMKMAct.class));
                            Intent main = new Intent(getApplicationContext(),HomePageUMKMAct.class);
                            
                            startActivity(main);
                            SelectTypeAct.activity.finish();
                            finish();
                        }else {
                            Toast.makeText(LoginUMKMAct.this, "Error! "+task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        }

                    }
                });
            }
        });
        lcreateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginUMKMAct.this, RegisterUMKMAct.class));
                finish();
            }
        });

    }
}
