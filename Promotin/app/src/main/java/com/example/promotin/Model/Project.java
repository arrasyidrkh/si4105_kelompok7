package com.example.promotin.Model;

public class Project {

    private String id, nama, email, owner, kategori, deskripsi, fee, urlFoto;

    public Project() {

    }

    public Project(String id, String nama, String email, String owner, String kategori, String deskripsi, String fee, String urlFoto) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.owner = owner;
        this.kategori = kategori;
        this.deskripsi = deskripsi;
        this.fee = fee;
        this.urlFoto = urlFoto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}


