package com.example.promotin;

public class UserUMKM {
    String email, handphone, id, nama_umkm, ownerUMKM, photoURL, project, search, status, type;

    public  UserUMKM(){}

    public UserUMKM(String email, String handphone, String id, String nama_umkm, String ownerUMKM, String photoURL, String project, String search, String status, String type) {
        this.email = email;
        this.handphone = handphone;
        this.id = id;
        this.nama_umkm = nama_umkm;
        this.ownerUMKM = ownerUMKM;
        this.photoURL = photoURL;
        this.project = project;
        this.search = search;
        this.status = status;
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_umkm() {
        return nama_umkm;
    }

    public void setNama_umkm(String nama_umkm) {
        this.nama_umkm = nama_umkm;
    }

    public String getOwnerUMKM() {
        return ownerUMKM;
    }

    public void setOwnerUMKM(String ownerUMKM) {
        this.ownerUMKM = ownerUMKM;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
