package com.example.promotin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginInfluencerAct extends AppCompatActivity {
    EditText kEmail, kPassword;
    Button kLogin;
    TextView kGoRegist;
    FirebaseAuth kAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_influencer);
        kEmail = findViewById(R.id.et_loginf_email);
        kPassword = findViewById(R.id.et_loginf_password);
        kLogin = findViewById(R.id.btn_loginf_login);
        kGoRegist = findViewById(R.id.tv_loginf_register);
        kAuth = FirebaseAuth.getInstance();

        kLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = kEmail.getText().toString().trim();
                String password = kPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    kEmail.setError("Email is Required.");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    kPassword.setError("Password is Required");
                    return;
                }
                if (password.length() < 5) {
                    kPassword.setError("Password must be more than 5 Characters");
                    return;
                }
                kLogin.setText("Loading...");
                //authenticate user
                kAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginInfluencerAct.this, "Login Success.", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(getApplicationContext(), HomepageInfluencerAct.class));
                            SelectTypeAct.activity.finish();
                            finish();
                        } else {
                            Toast.makeText(LoginInfluencerAct.this, "Error! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        kGoRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginInfluencerAct.this, RegisterInfluencerAct.class));
                finish();
            }
        });
    }
}