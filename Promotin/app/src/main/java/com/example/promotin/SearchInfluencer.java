package com.example.promotin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SearchInfluencer extends AppCompatActivity {
    private EditText searchInput;
    private Button searchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_influencer);

        searchButton = findViewById(R.id.buttonSearch);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToList();
            }
        });

        searchInput = findViewById(R.id.searchInfluencerInput);
    }

    protected void moveToList() {
        Intent intent = new Intent(this, ListInfluencer.class);
        intent.putExtra("keyword", this.searchInput.getText().toString());

        startActivity(intent);
    }
}
