package com.example.promotin;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.promotin.Model.Project;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class MakeProjectAct extends AppCompatActivity {

    private EditText etNamaProject, etKategori, etDeskripsi, etFee;
    private ImageView ivFotoProduk;
    private Button btnUpload;
    private TextView tvPilihFoto;

    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageReference storageReference;
    private DatabaseReference databaseReferenceProject;
    private DatabaseReference databaseReferenceUser;

    private FirebaseAuth firebaseAuth;

    private String username, useremail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_project);
        initViews();

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReferenceUser = FirebaseDatabase.getInstance().getReference("Users").child("UMKM");
        databaseReferenceProject = FirebaseDatabase.getInstance().getReference("project");
        storageReference = FirebaseStorage.getInstance().getReference("project");

        ivFotoProduk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pilihFoto();
            }
        });

        btnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama, kategori, deskripsi, fee;
                nama = etNamaProject.getText().toString().trim();
                kategori = etKategori.getText().toString().trim();
                deskripsi = etDeskripsi.getText().toString().trim();
                fee = etFee.getText().toString().trim();
                if (!nama.equals("") || !kategori.equals("") || !deskripsi.equals("") || !fee.equals("")){
                    ulpoadProject();
                }else{
                    Toast.makeText(MakeProjectAct.this, "Harap lengkapi semua data", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void ulpoadProject() {
        btnUpload.setText("Sedang mengupload...");
        if (imageUri != null) {
            final StorageReference fileReference = storageReference.child(System.currentTimeMillis() + "." + getFileExtension(imageUri));
            fileReference.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    fileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            //Toast.makeText(BuatProjectActivity.this, "Berhasil membuat project", Toast.LENGTH_SHORT).show();

                            final String uploadId, nama, email, kategori, deskripsi, fee, urlFoto;
                            uploadId = databaseReferenceProject.push().getKey();
                            nama = etNamaProject.getText().toString();
                            email = firebaseAuth.getCurrentUser().getEmail();
                            kategori = etKategori.getText().toString();
                            deskripsi = etDeskripsi.getText().toString();
                            fee = etFee.getText().toString();
                            urlFoto = uri.toString();

                            databaseReferenceUser.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String owner = dataSnapshot.child(firebaseAuth.getCurrentUser().getUid()).child("nama_umkm").getValue(String.class);
                                    Project project = new Project(uploadId, nama, email, owner, kategori, deskripsi, fee, urlFoto);

                                    databaseReferenceProject.child(uploadId).setValue(project);

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            Intent intent = new Intent(MakeProjectAct.this, SuccessMakeProjectAct.class);
                            intent.putExtra("successMessage", "Berhasil membuat project!");
                            startActivity(intent);
                            finish();

                        }
                    });
                }
            })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(MakeProjectAct.this, "Gagal mengupload barang", Toast.LENGTH_SHORT).show();
                            btnUpload.setText("Upload Project");
                        }
                    });
        } else {
            Toast.makeText(this, "Harap pilih foto", Toast.LENGTH_SHORT).show();
            btnUpload.setText("Upload Project");
        }
    }

    private void pilihFoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private String getFileExtension(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();

            tvPilihFoto.setText(null);
            Picasso.with(this).load(imageUri).into(ivFotoProduk);
            ivFotoProduk.setBackground(null);
            ivFotoProduk.setImageURI(imageUri);
        }
    }

    public void initViews() {
        etNamaProject = findViewById(R.id.et_buatproject_namaproject);
        etKategori = findViewById(R.id.et_buatproject_kategoriproject);
        etDeskripsi = findViewById(R.id.et_buatproject_deskripsiproject);
        etFee = findViewById(R.id.et_buatproject_feeproject);
        ivFotoProduk = findViewById(R.id.iv_buatproject_fotoproject);
        btnUpload = findViewById(R.id.btn_buatproject_upload);
        tvPilihFoto = findViewById(R.id.tv_buatproject_klikfoto);
    }

}
